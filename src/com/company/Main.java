package com.company;

public class Main {
    public static void main(String[] args) {
        int n1 = 0;
        int n2 = 1;
        int sum = 1;
        for (int i = 2; i < 10; i++) {
            sum = n1 + n2;
            System.out.println("Afiseaza suma: n1 + n2 = " + sum);
            n1 = n2;
            n2 = sum;
        }
    }
}

